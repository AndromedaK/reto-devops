#versión mas ligera de imagen para node
FROM node:10-alpine 
#Establecer modulo de trabajo con usuario no root (comando de linux)
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app 
#nos situamos en el Directorio de trabajo 
WORKDIR /home/node/app 
#Copiar archivos json 
COPY package*.json ./
#Nos aseguramos que ocupemos el usuario node no-root
USER node 
#Instalar los paquetes de los que depende la aplicacion
RUN npm install 
#Permisos para que el dueño de los archivos sean de propiedad del usuario node no-root
COPY --chown=node:node . .
#anotamos un puerto declarativo 
EXPOSE 3000
#script 
CMD ["npm","start"]

